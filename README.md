This is an archive of the F-Droid forums from the old, pre-2017 website.

Each time the [fdroid-website](https://gitlab.com/fdroid/fdroid-website) project is deployed
to f-droid.org, it will take a copy of the `forums/` directory from this repository and make
it available at https://f-droid.org/forums.

It was generated by using `wget` to scrape f-droid.org to get all of the posts from the
Wordpress bbpress forum as static HTML. This results in some limitations:
 * There are still plenty of broken links (e.g. to user profiles)
   but most links to forum topics should work.
 * It was scraped in July 2017, so all of the dates will be relative to then.
